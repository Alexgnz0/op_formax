@extends('purchase.layout')
@section('content')
<nav class="section-nav">
	Ordenes de pedido
</nav>
<div class="table text-center container">
	<table class="table">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Estado</th>
      <th scope="col">Valor</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>
  <tbody>
  	@foreach ($purchases as $purchase)
  		<th scope="row">{{$purchase->id}}</th>
      <td>{{$purchase->state}}</td>
      <td>{{$purchase->value}}</td>
      <td>
      	<ul>
      		<li><i class="fas fa-pencil-alt update" data-id="{{$purchase->id}}" data-uri='purchase_update' data-data="purchase_get_data"></i></li>
      		<li><a href="{{ route('GetPurchase') }}?id={{$purchase->id}}"><i class="fas fa-eye delete"></i></a></li>
      		<li><i class="fas fa-trash delete" data-id="{{$purchase->id}}" data-uri='purchase_delete'></i></li>
      	</ul>
      </td>
    </tr>
    
  	@endforeach
    </tbody>
</table>

</div>
@endsection
