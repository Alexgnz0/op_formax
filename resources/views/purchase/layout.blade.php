<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Orden de Pedido</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css" integrity="sha384-xxzQGERXS00kBmZW/6qxqJPyxW3UR0BPsL4c8ILaIWXva5kFi7TxkIIaMiKtqV1Q" crossorigin="anonymous">
</head>
<body>
	<div class="openpopup">
		<div class="addop">
			<i class="fas fa-user-plus"></i>
		</div>
	</div>
	<div class="bg-popup">
		<section class="confirm-popup bg-white">
			<h4>Desea agregar la accion?</h4>
			<br>
			<button type="button" class="btn btn-primary cancel">Cancelar</button>
			<button type="button" class="btn btn-secondary confirm">Confirmar</button>
		</section>
		<section class="add-popup">
			<i class="fas fa-times close-popup"></i>
			<form id="form" method="POST" action="" >
				<div class="text-center">
					<small class="text-danger " id="valid"></small>	
				</div>
				
				@csrf
				<input type="hidden" id="id" name="id">
				<input type="hidden" id="create" name="create" value="{{ route('PurchaseStore') }}">
				<input type="hidden" id="update" name="update" value="{{ route('PurchaseUpdate') }}">
				<div class="form-group">
					<label>Canal</label>
					<input required class="form-control" type="text" name="channel">
				</div>
				<div class="form-group">
					<label>Estado</label>
					<select class="form-control" name="state">
						<option value="Reservado">Reservado</option>
						<option value="Pendiente">Pendiente</option>
						<option value="En transito">En transito</option>
						<option value="Lista Para Recoger">Lista Para Recoger</option>
						<option value="Cerrada">Cerrada</option>
					</select>
				</div>
				<div class="form-group">
					<label>Valor</label>
					<input required class="form-control" type="number" name="value" placeholder="$1000">
				</div>
				<div class="form-group">
					<label>Descuento</label>
					<input required class="form-control" type="number" name="discount" placeholder="20%">
				</div>
				<div class="form-group">
					<label>Tipo de entrega</label>
					<select class="form-control" name="delivery">
						<option>Estandard</option>
						<option>Express</option>
					</select>
				</div>
				<div class="form-group">
					<label>Tipo de envio</label>
					<select class="form-control" name="shipping">
						<option>Estrega en tienda</option>
						<option>Entrega en domicilio</option>
					</select>
				</div>
			</form>
			<button class="sendAdd">Enviar</button>
		</section>
	</div>
	<header>
		<nav id="nav-mobile">
			<span class="open-nav"><i class="fas fa-bars"></i></span>
		</nav>
		<nav class="col-md-3" id="nav">
			<h3 class="text-center text-white pt-3">Logo o Titulo</h4>
				<ul>
					<li><a class="active" href="/"><span><i class="far fa-clipboard"></i></span>Ordenes de pedido</a></li>
					<li><a href=""><span><i class="fas fa-shopping-cart"></i></span>Carrito</a></li>
					<li><a href=""><span><i class="fas fa-users"></i></span>Clientes</a></li>
					<li><a href=""><span><i class="fas fa-shipping-fast"></i></span>Envios</a></li>
				</ul>
		</nav>
	</header>
	<div class="main-content">
			@yield('content')
	</div>

		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

		<script type="text/javascript" src="{{ asset('js/script.js') }}"></script>
	</body>
	</html>