@extends('products.layout')
@section('content')
<section class="row purchase-data">
	@foreach ($purchase as $val)
		
	<div class="col-md-6 col-12 text-center">
		<ul>
			<li>Canal: {{$val->channel}}</li>
			<li>Estado: {{$val->state}}</li>
			<li>Valor: {{$val->value}}</li>
		</ul>
	</div>
	<div class="col-md-6 col-12 text-center">
		<ul>
			<li>Descuento: {{$val->discount}}</li>
			<li>Envio: {{$val->delivery}}</li>
			<li>Entrega: {{$val->shipping}}</li>
		</ul>
	</div>

	@endforeach
</section>
<div class="table text-center container">
	<table class="table">
		<thead>
			<tr>
				<th scope="col">SKU</th>
				<th scope="col">Nombre</th>
				<th scope="col">Estado</th>
				<th scope="col">Acciones</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			@foreach ($products as $product)
			<th scope="row">{{$product->sku}}</th>
			<th scope="row">{{$product->name}}</th>
			<td>{{$product->name}}</td>
			<td>
				<ul>
					<li><i class="fas fa-trash delete" data-id="{{$product->id}}" data-uri='/product_delete'></i></li>
				</ul>
			</td>
		</tr>
			@endforeach
	</tbody>
</table>

</div>
@endsection