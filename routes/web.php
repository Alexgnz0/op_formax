<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
//Purchase
Route::get('/', 'PurchaseController@index');
Route::get('/purchase', 'PurchaseController@getPurchase')->name('GetPurchase');
Route::post('/purchase','PurchaseController@store')->name('PurchaseStore');
Route::post('/purchase_update','PurchaseController@update')->name('PurchaseUpdate');
Route::post('/purchase_delete', 'PurchaseController@destroy')->name('PurchaseDestroy');
Route::post('/purchase_get_data', 'PurchaseController@getData')->name('PurchaseGetData');
Route::post('/product', 'ProductController@store')->name('ProductStore');
Route::post('/product_delete', 'ProductController@destroy')->name('ProductDestroy');


//Product




