<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
	  protected $table = 'purchase_order';

      protected $fillable = [
        'id', 'channel', 'state', 'value', 'discount', 'delivery', 'shipping'
    ];  
}
