<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\Product;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchases = Purchase::all();
        return view('purchase.index', compact('purchases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
  
        Purchase::create($request->except('_token'));

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function show(Purchase $purchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {           
       $id = request('id');
       
        Purchase::where('id', $id)->update([
            'channel' => request('channel'),
            'state' => request('state'),
            'value' => request('value'),
            'discount' => request('discount'),
            'delivery' => request('delivery'),
            'shipping' => request('shipping')
        ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        $id = request('id');

        Purchase::where('id', $id)->delete();   

        return redirect('/'); 
    }
    public function getData(Purchase $purchase)
    {
        $id = request('id');

        $purchase = Purchase::where('id', $id)->get();   
        
        return $purchase;
    }

    public function GetPurchase(){
        $id = request('id');
        $products = Product::where('purchase_id', $id)->get();
        $purchase = Purchase::where('id', $id)->get();

        return view('products.index', compact('purchase', 'id', 'products'));
    }
}
