const nav = $('#nav')
$('.open-nav').on('click', function(){
	nav.toggleClass('active-nav')
})

const addParent = $('.add-popup').parent()
$('.close-popup').on('click', function(){
	addParent.fadeOut()
})

const bgPopUp = $('.bg-popup')
const addpopup = $('.add-popup') 
$('.addop').on('click', function(){
	bgPopUp.fadeIn()
	addpopup.fadeIn()
	$('#form').attr('action', $('#create').val())
	$('body').css({'overflow': 'hidden'})
	//$('#form input').val('')
})

$('.sendAdd').on('click', function(){

	if( $('#form')[0].checkValidity()) {
  // If the form is invalid, submit it. The form won't actually submit;
  // this will just cause the browser to display the native HTML5 error messages.

	$('.add-popup').addClass('popup_up')
	$('.confirm-popup').addClass('popup_down')
	}
	else{
		$('#valid').text('Complete todos los campos')
	}
})
$('.confirm').on('click', function(){
	$('#form').submit()
})
$('.cancel').on('click', function(){
	$('.confirm-popup').removeClass('popup_down')
	$('.add-popup').removeClass('popup_up')
})
$('.delete').on('click', function(){
	let dataset = $(this)[0].dataset
	delete_data(dataset.id, dataset.uri)
})

$('.update').on('click', function(){
	let dataset = $(this)[0].dataset
	$('#id').val(dataset.id)
	bgPopUp.fadeIn()
	addpopup.fadeIn()
	get_data(dataset.id, dataset.data)
	$('#form').attr('action', $('#update').val())

})

function delete_data(id, uri){
	$.ajax({
		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		},
    // En data puedes utilizar un objeto JSON, un array o un query string
    data: {
    	'id' : id,
    },
    //Cambiar a type: POST si necesario
    type: 'POST',
    // URL a la que se enviará la solicitud Ajax
    url: uri,
})
	.done(function( data, textStatus, jqXHR ) {
		if ( console && console.log ) {
			console.log( "La solicitud se ha completado correctamente." );
		}
		location.reload()
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "La solicitud a fallado: " +  textStatus);
		}
	});
}

function get_data(id, uri){
	$.ajax({
		headers: {

			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		},
    // En data puedes utilizar un objeto JSON, un array o un query string
    data: {
    	'id' : id,
    },
    //Cambiar a type: POST si necesario
    type: 'POST',
    // URL a la que se enviará la solicitud Ajax
    url: uri,
})
	.done(function( data, textStatus, jqXHR ) {
		if ( console && console.log ) {
			console.log( "La solicitud se ha completado correctamente." );
		}
		const form = $('#form')
		data = data[0]
		delete data.created_at
		delete data.updated_at
		for (var key in data) {
			$(`input[name=${key}]`).val(data[key])
			$(`select[name=${key}]`).val(data[key])
			console.log(`${key}:${data[key]}`)
		}
	})
	.fail(function( jqXHR, textStatus, errorThrown ) {
		if ( console && console.log ) {
			console.log( "La solicitud a fallado: " +  textStatus);
		}
	});
}
